import { createSlice } from "@reduxjs/toolkit";
import type { PayloadAction } from "@reduxjs/toolkit";

export interface IUser {
  id: string;
  login: string;
  name: string;
  age: number | null;
  password: string;
  isAdmin: boolean;
}
export interface IState {
  user: IUser;
}

const initialState: IState = {
  user: {
    id: "",
    login: "",
    name: "",
    age: null,
    password: "",
    isAdmin: false,
  },
};

export const userSlice = createSlice({
  name: "user",
  initialState,
  reducers: {
    addUser: (state, action: PayloadAction<IUser>) => {
      state.user = action.payload;
    },
  },
});

export const { addUser } = userSlice.actions;

export default userSlice.reducer;
